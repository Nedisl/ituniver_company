import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders, HttpParams} from '@angular/common/http';
import qs from 'qs';
import {environment} from '../environments/environment';

// const API_URL = 'http://46.161.39.213:4367';
const API_URL = 'http://193.176.78.104:1002';

@Injectable({providedIn: 'root'})
export class RestApiService {

  constructor(private http: HttpClient) {
  }

  getHeaders() {
    const token = localStorage.getItem('company_token');
    return token
      ? new HttpHeaders().set('Authorization', token)
      : null;
  }

  get(link: string) {
    return this
      .http
      .get(link, {
        headers: this.getHeaders()
      })
      .toPromise();
  }

  post(link: string, body: any) {
    return this
      .http
      .post(link, body, {
        headers: this.getHeaders()
      })
      .toPromise();
  }

  put(link: string, body: any) {
    return this
      .http
      .put(link, body, {
        headers: this.getHeaders()
      })
      .toPromise();
  }

  loginCompany(body: any) {
    return this
      .http
      .post(`${API_URL}/api/company/login`, body)
      .toPromise();
  }

  signupCompany(body: any) {
    return this
      .http
      .post(`${API_URL}/api/company/signup`, body)
      .toPromise();
  }

  uploadImage(apiUrl: string, body: any) {
    const headers = this.getHeaders();
    headers.append('Content-Type', 'multipart/form-data');
    headers.append('Accept', 'application/json');
    console.log(body);
    return this
      .http
      .post(`${apiUrl}/uploads/new.php`, body)
      .toPromise();
  }

  uploadVectorImage(apiUrl: string, body: any) {
    const headers = this.getHeaders();
    headers.append('Content-Type', 'multipart/form-data');
    headers.append('Accept', 'application/json');
    console.log(body);
    return this
      .http
      .post(`${apiUrl}/uploads/new_vector.php`, body)
      .toPromise();
  }

  updateCompany(body: any) {
    return this
      .http
      .put(`${API_URL}/api/company/profile/update`, body, {
        headers: this.getHeaders()
      })
      .toPromise();
  }

  updatePassword(body: any) {
    return this
      .http
      .put(`${API_URL}/api/company/profile/update/password`, body, {
        headers: this.getHeaders()
      })
      .toPromise();
  }
  updateStatus(body: any) {
    return this
      .http
      .put(`${API_URL}/api/course/update/status`, body, {
        headers: this.getHeaders()
      })
      .toPromise();
  }
  subscribe(body: any) {
    return this
      .http
      .post(`${API_URL}/api/subscription/subscribe`, body)
      .toPromise();
  }


  getCompanyProfile() {
    return this
      .http
      .get(`${API_URL}/api/company/profile`, {
        headers: this.getHeaders()
      })
      .toPromise();
  }

  updateAvatarHor(body: any) {
    return this
      .http
      .put(`${API_URL}/api/company/profile/update/avatar_hor`, body, {
        headers: this.getHeaders()
      })
      .toPromise();
  }

  updateAvatar(body: any) {
    return this
      .http
      .put(`${API_URL}/api/company/profile/update/avatar`, body, {
        headers: this.getHeaders()
      })
      .toPromise();
  }

  getCourseById(id) {
    return this
      .http
      .get(`${API_URL}/api/course/${id}`, {
        headers: this.getHeaders()
      })
      .toPromise();
  }

  getListenersForInstance(id) {
    return this
      .http
      .get(`${API_URL}/api/course/listeners/${id}`, {
        headers: this.getHeaders()
      })
      .toPromise();
  }


  addCourse(body: any) {
    return this
      .http
      .post(`${API_URL}/api/course/add`, body, {
        headers: this.getHeaders()
      })
      .toPromise();
  }

  addWeek(body: any) {
    return this
      .http
      .post(`${API_URL}/api/week/add`, body, {
        headers: this.getHeaders()
      })
      .toPromise();
  }

  addTopic(body: any) {
    return this
      .http
      .post(`${API_URL}/api/topic/add`, body, {
        headers: this.getHeaders()
      })
      .toPromise();
  }

  checkPassword(body: any) {
    return this
      .http
      .post(`${API_URL}/api/company/check/password`, body, {
        headers: this.getHeaders()
      })
      .toPromise();
  }

  updateCourse(body: any) {
    return this
      .http
      .post(`${API_URL}/api/course/update`, body, {
        headers: this.getHeaders()
      })
      .toPromise();
  }

  updateCourseLogo(body: any) {
    return this
      .http
      .post(`${API_URL}/api/course/update/logo`, body, {
        headers: this.getHeaders()
      })
      .toPromise();
  }

  updateCourseLecturerPhoto(body: any) {
    return this
      .http
      .post(`${API_URL}/api/course/update/lecturer_photo`, body, {
        headers: this.getHeaders()
      })
      .toPromise();
  }

  getAllCities() {
    return this
      .http
      .get(`${API_URL}/api/cities`, {
        headers: this.getHeaders()
      })
      .toPromise();
  }

  getCompanyCourses(companyId) {
    return this
      .http
      .get(`${API_URL}/api/course/company/${companyId}`, {
        headers: this.getHeaders()
      })
      .toPromise();
  }

  getCompanyCourseInstances(companyId) {
    return this
      .http
      .get(`${API_URL}/api/course/instances/company/${companyId}`, {
        headers: this.getHeaders()
      })
      .toPromise();
  }

  getWeeks(id) {
    return this
      .http
      .get(`${API_URL}/api/course/weeks/${id}`, {
        headers: this.getHeaders()
      })
      .toPromise();
  }

  getAllUniversities() {
    return this
      .http
      .get(`${API_URL}/api/universities`, {
        headers: this.getHeaders()
      })
      .toPromise();
  }
}
