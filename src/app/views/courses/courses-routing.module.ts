import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ListComponent } from './list.component';
import { AddComponent } from './add.component';
import { EditComponent } from './edit.component';
import {CurrentComponent} from './current.component';
import {AppliesComponent} from './applies.component';

const routes: Routes = [
  {
    path: '',
    data: {
      title: 'Курсы'
    },
    children: [
      {
        path: 'catalog',
        component: ListComponent,
        data: {
          title: 'Каталог'
        }
      },
      {
        path: 'current',
        component: CurrentComponent,
        data: {
          title: 'Текущие курсы'
        }
      },
      {
        path: 'add',
        component: AddComponent,
        data: {
          title: 'Добавить'
        }
      },
      {
        path: 'edit/:id',
        component: EditComponent,
        data: {
          title: 'Редактировать'
        }
      },
      {
        path: 'applies/:id',
        component: AppliesComponent,
        data: {
          title: 'Список заявок'
        }
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CoursesRoutingModule {}
