import {Component, OnInit} from '@angular/core';
import {DataService} from '../../data.service';
import {RestApiService} from '../../rest-api.service';
import {Router} from '@angular/router';
import { FormsModule } from '@angular/forms'; import { ReactiveFormsModule } from '@angular/forms';
import {ToastyService, ToastyConfig, ToastOptions, ToastData} from 'ngx-toasty';
import {UploadFileService} from '../../upload-file.service';
import {NgxSpinnerComponent, NgxSpinnerService} from 'ngx-spinner';


@Component({
  templateUrl: 'profile.component.html'
})
export class ProfileComponent implements OnInit {
  city_id: number;
  namep: string;
  emailp: string;
  phonep: string;
  avatar: string;
  avatar_hor: string;
  submitDisabled: boolean;
  isEmailValid: boolean;
  isNameCorrect: boolean;
  isPhoneCorrect: boolean;
  isCityCorrect: boolean;
  allCorrect: boolean;

  constructor (public data: DataService,
               private rest: RestApiService,
               private router: Router,
               private fileUploader: UploadFileService,
               private spinner: NgxSpinnerService) {

  }

  async ngOnInit() {
    this.isEmailValid = true;
    this.isNameCorrect = true;
    this.isPhoneCorrect = true;
    this.isCityCorrect = true;
    this.allCorrect = true;
    await this.spinner.show();
    await this.data.getCompanyProfile();
    await this.spinner.hide();
    this.city_id = this.data.user.city.id;
    this.namep = this.data.user.name;
    this.emailp = this.data.user.email;
    this.phonep = this.data.user.phone;
    this.avatar = 'http://it-univer43.catkov.beget.tech/uploads/' + this.data.user.avatar;
    this.avatar_hor = 'http://it-univer43.catkov.beget.tech/uploads/' + this.data.user.avatar_hor;
    console.log(this.data.user);
  }

  async update() {
    this.submitDisabled = true;
    // tslint:disable-next-line:max-line-length
    const re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    this.isEmailValid =  re.test(String(this.emailp).toLowerCase());
    this.isNameCorrect = (this.namep !== '');
    this.isPhoneCorrect = (this.phonep.length === 10);
    this.isCityCorrect = (this.city_id !== 0);
    this.submitDisabled = true;
    this.allCorrect = this.isEmailValid && this.isNameCorrect && this.isPhoneCorrect && this.isCityCorrect;

    if (this.allCorrect) {
      try {
        if (this.city_id !== 0) {
          await this
            .rest
            .updateCompany({
              name: this.namep,
              email: this.emailp,
              phone: this.phonep,
              city_id: this.city_id
            }).then((res) => {
              console.log(res);
              this
                .data
                .addToast('Данные успешно обновлены', '', 'success');
              this
                .router
                .navigate(['/dashboard']);
            });

        } else {
          this
            .data
            .addToast('Выберите город!', '', 'error');
        }
      } catch (error) {
        const message = error.error.meta.message;
        this
          .data
          .addToast(message, '', 'error');
      }
    }

    this.submitDisabled = false;
  }

  async fileChange(event) {
    const fileList: FileList = event.target.files;
    if (fileList.length > 0) {
      const file = fileList[0];
      const pictureUrl = await this
        .fileUploader
        .uploadVectorImage(file);

      try {
        await this
          .rest
          .updateAvatar({
            avatar: pictureUrl
          });
        this.avatar = 'http://it-univer43.catkov.beget.tech/uploads/' + pictureUrl;
        this.data.user.avatar = 'http://it-univer43.catkov.beget.tech/uploads/' + pictureUrl;
        this
          .data
          .success('Аватар обновлен');

        await this
          .data
          .getCompanyProfile();
      } catch (error) {
        this
          .data
          .error(error['message']);
      }
    }
  }
  async fileHorChange(event) {
    const fileList: FileList = event.target.files;
    if (fileList.length > 0) {
      const file = fileList[0];
      const pictureUrl = await this
        .fileUploader
        .uploadVectorImage(file);

      try {
        await this
          .rest
          .updateAvatarHor({
            avatar_hor: pictureUrl
          });
        this.avatar_hor = 'http://it-univer43.catkov.beget.tech/uploads/' + pictureUrl;
        this.data.user.avatar_hor = 'http://it-univer43.catkov.beget.tech/uploads/' + pictureUrl;
        this
          .data
          .success('Аватар обновлен');

        await this
          .data
          .getCompanyProfile();
      } catch (error) {
        this
          .data
          .error(error['message']);
      }
    }
  }


}
