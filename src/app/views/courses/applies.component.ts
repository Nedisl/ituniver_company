import { Component, OnInit } from '@angular/core';
import { getStyle, rgbToHex } from '@coreui/coreui/dist/js/coreui-utilities';
import {DataService} from '../../data.service';
import {RestApiService} from '../../rest-api.service';
import {ActivatedRoute, Router} from '@angular/router';
import {NgxSpinnerService} from 'ngx-spinner';

@Component({
  templateUrl: 'applies.component.html'
})


export class AppliesComponent implements OnInit {
  listeners: any = null;
  sub: any;
  id: number;
  constructor (public data: DataService,
               private rest: RestApiService,
               private router: Router,
               private spinner: NgxSpinnerService,
               private route: ActivatedRoute) {
  }

  async ngOnInit() {
    if (this.listeners == null) {
      await this.spinner.show();
    }
    this.sub = this.route.params.subscribe(async params => {
      this.id = +params['id'];
      await this.data.getCompanyProfile();
      this.listeners = await this.rest.getListenersForInstance(+params['id']);
      await this.spinner.hide();
      console.log(this.listeners);
    });
  }

  async updateStatus(listener) {
    console.log(listener.status + ' ' + listener.listener.id);
    this
      .rest
      .updateStatus({
        id: listener.course_instance_id,
        listener_id: listener.listener_id,
        status: listener.status
      }).then((res) => {
      console.log(res);
      this
        .data
        .addToast('Статус пользователя успешно обновлен', '', 'success');
    });
  }
}

