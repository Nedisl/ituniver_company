import {Component, OnInit} from '@angular/core';
import {DataService} from '../../data.service';
import {RestApiService} from '../../rest-api.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-dashboard',
  templateUrl: 'login.component.html'
})


export class LoginComponent implements OnInit {
  submitDisabled: boolean;
  email: string;
  password: string;
  isEmailValid: boolean;

  constructor (public data: DataService,
              private rest: RestApiService,
              private router: Router) {

  }

  async ngOnInit() {
    this.isEmailValid = true;
  }

  async login() {
    this.submitDisabled = true;
    // tslint:disable-next-line:max-line-length
    const re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    this.isEmailValid = re.test(String(this.email).toLowerCase());

    if (this.isEmailValid) {
      try {
        const res = await this
          .rest
          .loginCompany({
            email: this.email,
            password: this.password,
          });
        if (res['meta'].success) {
          localStorage.setItem('company_token', res['data'].token);
          this
            .data
            .addToast('Вы успешно авторизованы', '', 'success');
          this
            .router
            .navigate(['/dashboard']).then(() => {
          });
        } else {
          this
            .data
            .addToast('Неверно введен логин или пароль', '', 'error');
        }
      } catch (error) {
        const message = error.error.meta.message;
        this
          .data
          .addToast(message, '', 'error');
      }
    }

    this.submitDisabled = false;
  }

}
