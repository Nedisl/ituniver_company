import {Component, OnInit} from '@angular/core';
import {DataService} from '../../data.service';
import {RestApiService} from '../../rest-api.service';
import {ToastyService, ToastyConfig, ToastOptions, ToastData} from 'ngx-toasty';
import {Router} from '@angular/router';
import {NgxSpinnerService} from 'ngx-spinner';

@Component({
  selector: 'app-dashboard',
  templateUrl: 'register.component.html'
})
export class RegisterComponent implements OnInit {
  name: string;
  email: string;
  password: string;
  repeat_password: string;
  city_id: number;
  phone: string;

  isEmailValid: boolean;
  isPasswordMatch: boolean;
  isNameCorrect: boolean;
  isPhoneCorrect: boolean;
  isCityCorrect: boolean;
  allCorrect: boolean;

  submitDisabled = false;
  constructor(public data: DataService,
              private rest: RestApiService,
              private router: Router,
              private spinner: NgxSpinnerService) {
  }

  async ngOnInit() {
    this.city_id = 0;
    this.name = '';
    this.email  = '';
    this.password = '';
    this.repeat_password = '';
    this.phone = '';
    this.isEmailValid = true;
    this.isPasswordMatch = true;
    this.isNameCorrect = true;
    this.isPhoneCorrect = true;
    this.isCityCorrect = true;
    this.allCorrect = true;
  }

  async register() {
    this.submitDisabled = true;

    // tslint:disable-next-line:max-line-length
    const re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    this.isEmailValid =  re.test(String(this.email).toLowerCase());
    this.isPasswordMatch = ((this.password === this.repeat_password) && this.password !== '');
    this.isNameCorrect = (this.name !== '');
    this.isPhoneCorrect = (this.phone.length === 10);
    this.isCityCorrect = (this.city_id !== 0);
    this.submitDisabled = true;
    this.allCorrect = this.isEmailValid && this.isPasswordMatch && this.isNameCorrect && this.isPhoneCorrect && this.isCityCorrect;

    if (this.allCorrect) {
      try {
        if (this.city_id !== 0) {
          if (this.password === this.repeat_password) {
            await this.spinner.show();
            await this
              .rest
              .signupCompany({
                name: this.name,
                email: this.email,
                password: this.password,
                phone: this.phone,
                city_id: this.city_id,
              }).then(async (res) => {
                await this
                  .data
                  .addToast('Вы успешно зарегистрированы', '', 'success');
                await this.spinner.hide();
                await this.login();
              });
          } else {
            this
              .data
              .addToast('Пароли не совпадают!', '', 'error');
          }
        } else {
          this
            .data
            .addToast('Выберите город!', '', 'error');
        }
      } catch (error) {
        const message = error.error.meta.message;
        this
          .data
          .addToast(message, '', 'error');
      }
    }

    this.submitDisabled = false;
  }

  async login() {

    try {
      const res = await this
        .rest
        .loginCompany({
          email: this.email,
          password: this.password,
        });
      if (res['meta'].success) {
        localStorage.setItem('company_token', res['data'].token);
        this
          .data
          .addToast('Вы успешно авторизованы', '', 'success');
        this
          .router
          .navigate(['/dashboard']).then(() => {
        });
      } else {
        this
          .data
          .addToast('Неверно введен логин или пароль', '', 'error');
      }
    } catch (error) {
      const message = error.error.meta.message;
      this
        .data
        .addToast(message, '', 'error');
    }
  }

}
