// Angular
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';

import { ListComponent } from './list.component';
import { AddComponent } from './add.component';
import { EditComponent } from './edit.component';

// Theme Routing
import { CoursesRoutingModule } from './courses-routing.module';
import {FormsModule} from '@angular/forms';
import {CurrentComponent} from './current.component';
import {AppliesComponent} from './applies.component';
import {AutofocusModule} from 'angular-autofocus-fix';

@NgModule({
  imports: [
    CommonModule,
    CoursesRoutingModule,
    FormsModule,
    AutofocusModule
  ],
  declarations: [
    ListComponent,
    AddComponent,
    EditComponent,
    CurrentComponent,
    AppliesComponent
  ]
})
export class CoursesModule { }
