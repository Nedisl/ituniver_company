import {Component, OnInit} from '@angular/core';
import {DataService} from '../../data.service';
import {RestApiService} from '../../rest-api.service';
import {Router} from '@angular/router';
import { FormsModule } from '@angular/forms'; import { ReactiveFormsModule } from '@angular/forms';
import {ToastyService, ToastyConfig, ToastOptions, ToastData} from 'ngx-toasty';
import {UploadFileService} from '../../upload-file.service';


@Component({
  templateUrl: 'add.component.html'
})

export class AddComponent implements OnInit {
  university_id: number;
  submitDisabled: boolean;
  name: string;
  description: string;
  volume: number;
  logo: string;
  logoServer: string;
  lecturerPhoto: string;
  lecturerPhotoServer: string;
  lecturerName: string;
  lecturerAbout: string;
  isNameCorrect: boolean;
  isDescriptionCorrect: boolean;
  isUniversityCorrect: boolean;
  isLecturerNameCorrect: boolean;
  isLecturerAboutCorrect: boolean;
  isTopicValid = true;
  allCorrect: boolean;
  weeks: any;
  requirements: any;

  constructor (public data: DataService,
               private rest: RestApiService,
               private router: Router,
               private fileUploader: UploadFileService) {
  }

  async  ngOnInit() {
    this.weeks = [];
    this.requirements = [];
    this.isNameCorrect = true;
    this.isDescriptionCorrect = true;
    this.isUniversityCorrect = true;
    this.isLecturerNameCorrect = true;
    this.isLecturerAboutCorrect = true;
    this.allCorrect = true;
    await this.data.getCompanyProfile();
    this.university_id = 1;
    this.lecturerPhoto = 'http://it-univer43.catkov.beget.tech/uploads/images/ava.jpeg';
    this.logo = 'http://it-univer43.catkov.beget.tech/uploads/images/ava.jpeg';
    this.lecturerPhotoServer = 'images/ava.jpeg';
    this.logoServer = 'images/ava.jpeg';
    this.name = '';
    this.description = '';
    this.lecturerName = '';
    this.lecturerAbout = '';
    this.volume = 0;
  }
  async logoChange(event) {
    const fileList: FileList = event.target.files;
    if (fileList.length > 0) {
      const file = fileList[0];
      const pictureUrl = await this
        .fileUploader
        .uploadVectorImage(file);

      try {
        this.logo = 'http://it-univer43.catkov.beget.tech/uploads/' + pictureUrl;
        this.logoServer = pictureUrl;
        this
          .data
          .success('Логотип добавлен');
      } catch (error) {
        this
          .data
          .error(error['message']);
      }
    }
  }

  addWeek() {
    this.weeks.push({ name: '', deleted: false, topics: [] });
  }

  addReq() {
    this.requirements.push({ name: '', deleted: false });
  }

  addTopic(week) {
    week.topics.push({ name: '', deleted: false });
  }

  checkWeeks() {
    if (this.weeks.some(e => e.name === '' && e.deleted === false)) {
      return true;
    }

    return false;
  }

  checkReqs() {
    if (this.requirements.some(e => e.name === '' && e.deleted === false)) {
      return true;
    }

    return false;
  }

  checkTopics(topics) {
    if (topics.some(e => e.name === '' && e.deleted === false)) {
      this.isTopicValid = false;
      return true;
    }

    this.isTopicValid = true;
    return false;
  }

  async photoChange(event) {
    const fileList: FileList = event.target.files;
    if (fileList.length > 0) {
      const file = fileList[0];
      const pictureUrl = await this
        .fileUploader
        .uploadImage(file);
      try {
        this.lecturerPhoto = 'http://it-univer43.catkov.beget.tech/uploads/' + pictureUrl;
        this.lecturerPhotoServer = pictureUrl;
        this
          .data
          .success('Фото лектора добавлено');
      } catch (error) {
        this
          .data
          .error(error['message']);
      }
    }
  }

  async addCourse() {
    this.submitDisabled = true;

    this.isNameCorrect = (this.name !== '');
    this.isLecturerAboutCorrect = (this.lecturerAbout !== '');
    this.isDescriptionCorrect = (this.description !== '');
    this.isUniversityCorrect = (this.university_id !== 0);
    this.isLecturerNameCorrect = (this.lecturerName !== '');
    this.allCorrect = this.isNameCorrect && this.isDescriptionCorrect &&
      this.isUniversityCorrect && this.isLecturerNameCorrect && this.isLecturerAboutCorrect;

    if (this.allCorrect) {
      try {
        if (this.university_id !== 0) {
          await this
            .rest
            .addCourse({
              name: this.name,
              description: this.description,
              university_id: 1,
              lecturer_name: this.lecturerName,
              lecturer_photo: this.lecturerPhotoServer,
              logo: this.logoServer,
              lecturer_about: this.lecturerAbout,
              volume: this.volume,
              weeks: { weeks: this.weeks.filter(function(el) { return el.deleted !== true; }) },
              requirements: { requirements: this.requirements.filter(function(el) { return el.deleted !== true; }) }
            }).then((res) => {
              console.log(res);
              this
                .data
                .addToast('Курс успешно добавлен', '', 'success');
              this
                .router
                .navigate(['/courses/catalog']);
            });

        } else {
          this
            .data
            .addToast('Выберите университет!', '', 'error');
        }
      } catch (error) {
        const message = error.error.meta.message;
        this
          .data
          .addToast(message, '', 'error');
      }
    }

    this.submitDisabled = false;
  }


}
